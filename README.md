# SpriteCloud Automation Assignment

This project is using Selenium(https://www.selenium.dev/) and TestNG(https://testng.org/doc/).

## Quick Start & Documentation

## How to run the tests locally
Using the command line: mvn test

## How to run the test in a CI/CD pipeline
Navigate to the project on Gitlab.com and click on CI/CD in the sidebar. Then click on "Run Pipeline".

## Has a link to the results in Calliope.pro
https://app.calliope.pro/reports/110741/public/8da28b18-6893-40e0-a069-7e6055cb71c0


## What you used to select the scenarios, what was your approach?
The approach taken in selecting the functionality to cover with the automated tests was based on an MVP of the book
store, with the most important scenario being "I want to delete all books in my collection" as this uses multiple methods of the application.
As the search feature is the core feature for the book store, this was automated first. A login scenario was also 
selected as this feature is the first touch-point of the application for users.  


## Why are they the most important
Please see above.


## What could be the next steps to your project
Further steps are initially to complete the testing for all business scenarios. The OptionsManager can be configured to automatically detect when the code is running locally on in GitLab. 
Non-functional feature testing can be added to the solution with the use of ZED Attack Proxy to perform security scans on open APIs. 
All passwords used should be encrypted in order to ensure security. This can be done using a manual encrypting passwords and using a decryption method to decrypt these passwords.
